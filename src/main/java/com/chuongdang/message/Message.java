package com.chuongdang.message;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Chuong Dang, University of Information and Technology, HCMC Vietnam,
 *         Faculty of Computer Network and Telecommunication created on 3/2/15.
 */
@XmlRootElement(name = "message")
@XmlType(propOrder = { "type", "info", "content"})
public class Message {
 

    private MessageType type;

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public MessageBody getContent() {
        return content;
    }

    public void setContent(MessageBody content) {
        this.content = content;
    }

    private String info;
    
    private MessageBody content;
    
    public Message() {
        /* Default-constructor is required by jaxb lib */
    }
    
    public Message(MessageType type, String info, MessageBody content) {
        this.type = type;
        this.info = info;
        this.content = content;
    }
    
    


}
