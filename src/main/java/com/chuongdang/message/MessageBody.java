package com.chuongdang.message;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.net.InetAddress;

/**
 * @author Chuong Dang, University of Information and Technology, HCMC Vietnam,
 *         Faculty of Computer Network and Telecommunication created on 2/28/15.
 */
@XmlRootElement(name = "content")
@XmlType(propOrder = { "ip", "port"})

public class MessageBody {
    
    private String ip;
    
    private int port;
    
    public MessageBody() {
        
        
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public MessageBody(String ipAddr, int port) {
        this.ip = ipAddr;
        this.port = port;
    }


    public int getPort() {
        return port;
    }

    public void setPort(int _port) {
        this.port = _port;
    }

}
