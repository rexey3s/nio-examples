package com.chuongdang.message;

/**
 * @author Chuong Dang, University of Information and Technology, HCMC Vietnam,
 *         Faculty of Computer Network and Telecommunication created on 2/28/15.
 */
public enum MessageType {
    notify("notify"),
    request("request"),
    response("response");
    
    final String fullname;

    MessageType(String s) {
        fullname = s;
    }

    @Override
    public String toString() {
        return fullname;
    }
}
