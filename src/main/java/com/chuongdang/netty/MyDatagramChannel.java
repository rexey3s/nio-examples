package com.chuongdang.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramChannel;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.CharsetUtil;
import io.netty.util.NetUtil;
import io.netty.util.ReferenceCountUtil;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.util.Random;

/**
 * @author Chuong Dang, University of Information and Technology, HCMC Vietnam,
 *         Faculty of Computer Network and Telecommunication created on 2/27/15.
 */
public class MyDatagramChannel {

    private static final int PORT = Integer.parseInt(System.getProperty("port", "5102"));

    public static void main(String[] args) throws Exception {
        EventLoopGroup group = new NioEventLoopGroup();

        try {
            Bootstrap udpBootstrap = new Bootstrap();
            udpBootstrap.group(group)
                    .channel(NioDatagramChannel.class)
                    .option(ChannelOption.IP_MULTICAST_IF, NetUtil.LOOPBACK_IF)
                    .option(ChannelOption.SO_REUSEADDR, true)
                    .handler(new SimpleChannelInboundHandler<Object>() {

                        @Override
                        protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
                            ByteBuf in = (ByteBuf) msg;
                            System.out.println(msg);
                            try {
                                while (in.isReadable()) {
                                    System.out.print((char) in.readByte());
                                    System.out.flush();
                                }
                            } finally {
                                ReferenceCountUtil.release(msg);
                            }
                        }
                    });
            udpBootstrap.localAddress(5102);

//            b.bind(PORT).sync().channel().closeFuture().await();
            DatagramChannel datagramChannel = (DatagramChannel) udpBootstrap.bind().sync().channel();
            datagramChannel.joinGroup(new InetSocketAddress("239.255.1.111", 5102), NetUtil.LOOPBACK_IF).sync();
            datagramChannel.write(Unpooled.copiedBuffer("Where there is love there is life".getBytes())).sync();

        } finally {
            group.shutdownGracefully();
        }
    }

}
