package com.chuongdang.nio;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;

import com.chuongdang.jaxb.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamWriter;

/**
 * @author Chuong Dang, University of Information and Technology, HCMC Vietnam,
 *         Faculty of Computer Network and Telecommunication created on 2/11/15.
 */
public class DatagramChannelExample {
    private static int BUFF_SZ = 1024;
    public static void main(String args[]) throws IOException, InterruptedException, JAXBException {

        NetworkInterface ni = NetworkInterface.getByName("wlan0");
        InetAddress group = InetAddress.getByName("239.255.1.111");
        
        final DatagramChannel dc = DatagramChannel.open(StandardProtocolFamily.INET)
                .setOption(StandardSocketOptions.SO_REUSEADDR, true)
                .bind(new InetSocketAddress(5102))
                .setOption(StandardSocketOptions.IP_MULTICAST_IF, ni);
        final DatagramChannel dc1 = DatagramChannel.open(StandardProtocolFamily.INET)
                .setOption(StandardSocketOptions.SO_REUSEADDR, true)
                .bind(new InetSocketAddress(5102))
                .setOption(StandardSocketOptions.IP_MULTICAST_IF, ni);
        dc.join(group, ni);
        dc1.join(group, ni);

        Thread t = new Thread() {
            @Override
            public void run() {
                ByteBuffer in = ByteBuffer.allocate(BUFF_SZ);
                try {
                    dc.receive(in);
                    in.flip();
                    byte[] bytes = new byte[in.limit()];
                    in.get(bytes);
                    JAXBContext context = JAXBContext.newInstance(Bookstore.class);
                    Unmarshaller um = context.createUnmarshaller();
                    StringReader reader = new StringReader(new String(bytes));
                    Bookstore myBStore = (Bookstore) um.unmarshal(reader); 
                    
                    ArrayList<Book> list = myBStore.getBooksList();
                    for (Book book : list) {
                        System.out.println("Book: " + book.getName() + " from "
                                + book.getAuthor());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        t.start();
        /*
        Thread t1 = new Thread() {
            @Override
            public void run() {
                ByteBuffer in = ByteBuffer.allocate(BUFF_SZ);
                try {3
                    dc1.receive(in);
                    in.flip();
                    byte[] bytes = new byte[in.limit()];
                    in.get(bytes);
                    System.out.println(new String(bytes));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
        t1.start();
        */



        Thread.sleep(100);
        
        Bookstore myStore = buildBookstore();
        // create JAXB context and instantiate marshaller
        JAXBContext context = JAXBContext.newInstance(Bookstore.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringWriter writer = new StringWriter();
        m.marshal(myStore, writer);

        ByteBuffer bb2 = ByteBuffer.wrap(writer.toString().getBytes("utf8"));

        dc.send(bb2, new InetSocketAddress(group, 5102));

//        Thread.sleep(100);
//        dc.close();
//        dc1.close();
    }
    
    private static Bookstore buildBookstore() {
        ArrayList<Book> bookList = new ArrayList<Book>();

        // create books
        Book book1 = new Book();
        book1.setIsbn("978-0060554736");
        book1.setName("The Game");
        book1.setAuthor("Neil Strauss");
        book1.setPublisher("Harpercollins");
        bookList.add(book1);

//        Book book2 = new Book();
//        book2.setIsbn("978-3832180577");
//        book2.setName("Feuchtgebiete");
//        book2.setAuthor("Charlotte Roche");
//        book2.setPublisher("Dumont Buchverlag");
//        bookList.add(book2);

        // create bookstore, assigning book
        Bookstore bookstore = new Bookstore();
        bookstore.setName("Fraport Bookstore");
        bookstore.setLocation("Frankfurt Airport");
        bookstore.setBookList(bookList);

        return bookstore;        
    }
}
