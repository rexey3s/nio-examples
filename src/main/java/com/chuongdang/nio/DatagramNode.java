package com.chuongdang.nio;

import com.chuongdang.message.Message;
import com.chuongdang.message.MessageBody;
import com.chuongdang.message.MessageType;

import javax.xml.bind.*;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.channels.spi.SelectorProvider;
import java.util.Iterator;

/**
 * @author Chuong Dang, University of Information and Technology, HCMC Vietnam,
 *         Faculty of Computer Network and Telecommunication created on 2/12/15.
 */
public class DatagramNode {
    private static final int DEFAULT_PORT = 5102;
    private static final int READ_BUFFER_SIZE = 0x10000; // 512 Kb
    private static final int WRITE_BUFFER_SIZE = 0x10000; // 512 Kb
    
    private final ByteBuffer readBuffer = ByteBuffer.allocate(READ_BUFFER_SIZE);
    private final ByteBuffer writeBuffer = ByteBuffer.allocate(WRITE_BUFFER_SIZE);
    
    private MembershipKey membershipKey;
    private InetAddress multicastGroup;
    private static Selector selector;
    private final DatagramChannel datagramChannel;
    
    private int _port;
    
    public DatagramNode(NodeRecord record, String interfaceName, InetAddress group) throws IOException {
        this(record, interfaceName, DEFAULT_PORT, group);
        
    }
    public DatagramNode(NodeRecord record, String interfaceName, int port, InetAddress group) throws IOException {
        _port = port;
        multicastGroup = group;
        NetworkInterface ni = NetworkInterface.getByName(interfaceName);
        selector = SelectorProvider.provider().openSelector();
        
        datagramChannel = DatagramChannel.open(StandardProtocolFamily.INET)
                .setOption(StandardSocketOptions.SO_REUSEADDR, true)
                .bind(new InetSocketAddress(_port))
                .setOption(StandardSocketOptions.IP_MULTICAST_IF, ni);
        
        datagramChannel.configureBlocking(false);
        datagramChannel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE, record);
        membershipKey = datagramChannel.join(multicastGroup, ni);
    }
  
    
    public void read() {
        while (true) {
            try {
                selector.select();
                Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
                while (selectedKeys.hasNext()) {
                    SelectionKey key = selectedKeys.next();
                    selectedKeys.remove();
                    if (!key.isValid()) {
                        continue;
                    }
                    // Check what event is available and deal with it
                    if (key.isReadable()) {
                        handleRead(key);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
    private void handleRead(SelectionKey key) throws IOException, JAXBException {
        DatagramChannel dc = (DatagramChannel) key.channel();

        readBuffer.clear();
        dc.receive(readBuffer);
        readBuffer.flip();
        byte[] bytes = new byte[readBuffer.limit()];
        readBuffer.get(bytes);
        
        JAXBContext context = JAXBContext.newInstance(Message.class);
        Unmarshaller um = context.createUnmarshaller();
        StringReader reader = new StringReader(new String(bytes));
        Message msg = (Message) um.unmarshal(reader);

        MessageBody content = msg.getContent();
        System.out.println("Server addr: " +content.getIp().toString()+":"+content.getPort());
    }
    
    private void handleWrite(SelectionKey key) throws JAXBException, IOException {
        DatagramChannel dc = (DatagramChannel) key.channel();
        
//        Bookstore myStore = buildBookstore();
//        JAXBContext context = JAXBContext.newInstance(Bookstore.class);
//        Marshaller m = context.createMarshaller();
//        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//        StringWriter writer = new StringWriter();
//        m.marshal(myStore, writer);
        
//        writeBuffer.clear();
//        writeBuffer.flip();
//
//        dc.send(ByteBuffer.wrap(writer.toString().getBytes("utf8")),
//                new InetSocketAddress(multicastGroup, _port));
    }
    
    public void send(ByteBuffer payload) throws IOException {
        selector.select();
        Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
        while (selectedKeys.hasNext()) {
            SelectionKey key = selectedKeys.next();
            selectedKeys.remove();
            if (!key.isValid()) {
                continue;
            }
            // Check what event is available and deal with it
            if (key.isWritable()) {
                DatagramChannel dc = (DatagramChannel) key.channel();
                dc.send(payload, new InetSocketAddress(multicastGroup, _port));
            }
        }

    }
    
    
    public static Message buildSampleMessage() throws UnknownHostException, JAXBException {
        return new Message(MessageType.notify, "serverinfo",
                new MessageBody("10.0.0.1", 5102 ));
    }
    
    public static void main(String[] args) throws IOException, InterruptedException, JAXBException {
        
        DatagramNode node = new DatagramNode(new NodeRecord("F1",new InetSocketAddress(DEFAULT_PORT)) 
                ,"wlan0", InetAddress.getByName("239.255.1.111"));
        DatagramNode node2 = new DatagramNode(new NodeRecord("F2",new InetSocketAddress(DEFAULT_PORT))
                ,"wlan0", InetAddress.getByName("239.255.1.111"));
        Thread t = new Thread() {
            @Override
            public void run() {
                node2.read();
            }
        };
        t.start();
        Thread.sleep(100);
        Message msg = buildSampleMessage();
        JAXBContext context = JAXBContext.newInstance(Message.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringWriter writer = new StringWriter();
        
        m.marshal(msg, System.out);
        m.marshal(msg, writer);
        node.send(ByteBuffer.wrap(writer.toString().getBytes("utf8")));
    }
    static class NodeRecord {
        private SocketAddress address;
        private String nodeName;

        public SocketAddress getAddress() {
            return address;
        }

        public String getNodeName() {
            return nodeName;
        }

        private static ByteBuffer buffer = ByteBuffer.allocate(READ_BUFFER_SIZE);
        public NodeRecord(String name, SocketAddress address) {
            this.nodeName = name;
            this.address = address;
        }
    }
}
